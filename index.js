const express = require("express");
const { Pool } = require("pg");

/* Pool is faster than creating and freeing
 * file descriptors, since it preallocates them
 */
const pool = new Pool({
  host: "localhost",
  port: 5432,
  user: "node",
  password: "node-password123",
  database: "users", // database name
});

const app = express();

const appPort = process.env.PORT | 5000;

app.get("/", async (req, res) => {
  const con = await pool.connect();
  // schema_name.table_name
  const users = await con.query("SELECT * FROM users.users");

  const response = { rows: users.rows };

  res.status(200).send(response);
});

app.listen(appPort, () => {
  console.log(`Express running at port ${appPort}`);
});
