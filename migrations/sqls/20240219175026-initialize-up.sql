-- Create new schema
CREATE SCHEMA IF NOT EXISTS users;

-- Create new users table within users schema
CREATE TABLE IF NOT EXISTS users.users(
    id INT PRIMARY KEY,
    name VARCHAR(50)
);