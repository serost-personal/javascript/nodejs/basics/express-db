#!/bin/bash
docker compose -f docker-compose.yaml up -d --force-recreate --build

echo Waiting till PostgreSQL is capable of reciving connections.
sleep 5 # easiest way, other way would be retries

psql "postgres://postgres:password123@localhost:5432/postgres" \
    -f ./containers/postgresql/init.sql
echo done.
