-- Init with "initialize" migration

-- Here is how you can write PostgreSQL scripts:

-- Create database (initialy connected to "postgres" default database)
CREATE USER node WITH PASSWORD 'node-password123';
CREATE DATABASE users WITH OWNER node;

-- Change newly created database
\c "host=localhost port=5432 dbname=users user=node password=node-password123"

-- Create new schema
CREATE SCHEMA IF NOT EXISTS users;
CREATE TABLE IF NOT EXISTS users.users(
    id INT PRIMARY KEY,
    name VARCHAR(50)
);

-- Insert some placeholder users
INSERT INTO users.users (id, name) VALUES (1, 'John'), (2, 'Jane');