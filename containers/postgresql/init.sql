-- Bootstrap database

-- Create database (initialy connected to "postgres" default database)
CREATE USER node WITH PASSWORD 'node-password123';
CREATE DATABASE users WITH OWNER node;